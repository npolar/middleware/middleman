// Package middleman defines interfaces for middleware configuration and
// linking. It aims to make it easy to load and configure multiple middlewares
// in a modular and re-usable manner. Registration and Loading logic was
// inspired by Golangs "database/sql" package.
package middleman

import (
	"encoding/json"
	"net/http"
	"sort"
	"sync"
)

// Conf defines the configuration structure used by middleman middlewares.
type Conf struct {
	Name    string          `json:"name"`    // Middleware name
	Options json.RawMessage `json:"options"` // Middleware options as JSON
}

// Middleware defines a new interface for loading middleware functions into the
// http handler stack. It defines one method called Load.
type Middleware interface {
	// Configure takes configuration in the Conf format. It tries to load the
	// middleware by name and initializes it with the provided options.
	// Arguments:
	//	- json.RawMessage : JSON specifying which middleware and setup to use.
	// Output:
	//	- Link : a Link interface implementation.
	Configure(json.RawMessage) Link
}

// Link defines the interface you need to implement in order to become part of
// the middleware chain.
type Link interface {
	// Wrap takes the original http.HandlerFunc and wraps it with the configured
	// middleware handler.
	// Arguments:
	//	- http.HandlerFunc : The original http.HandlerFunc to be wrapped.
	// Output:
	//	- http.HandlerFunc : The original http.HandlerFunc wrapped with the
	//		middlewares handler logic.
	Wrap(http.HandlerFunc) http.HandlerFunc
}

var (
	middlewareMu sync.RWMutex
	middlewares  = make(map[string]Middleware)
)

// RegisterMiddleware adds a new middleware to the list of available middleware
// implementations for the http stack.
//	- name : The name we want to access the middleware with.
//	- m    : An implementation of the Middleware interface.
func RegisterMiddleware(name string, m Middleware) {
	middlewareMu.Lock()
	defer middlewareMu.Unlock()
	prefix := "MiddlwareRegisteration"
	if m == nil {
		panic(prefix + ": Provided middleware is nil")
	}
	if _, dup := middlewares[name]; dup {
		panic(prefix + ": Called with existing middlware name ->" + name)
	}
	middlewares[name] = m
}

// Middlewares returns a storted list of available middlewares.
func Middlewares() (list []string) {
	middlewareMu.Lock()
	defer middlewareMu.Unlock()
	for name := range middlewares {
		list = append(list, name)
	}
	sort.Strings(list)
	return
}

// Load the middleware with the given name and chain it to the provided
// http.HandlerFunc. It uses the opts JSON to configure the middleware.
func Load(name string, fn http.HandlerFunc, opts json.RawMessage) http.HandlerFunc {
	m, available := middlewares[name]
	if available {
		fn = m.Configure(opts).Wrap(fn)
	}
	return fn
}

// LoadChain initializes multiple middlewares at once given a middleman.Conf
// slice. It returnes the wrapped http.HandlerFunc. Middlewares are added to the
// chain in reverse order to make sure that the execution order matches the
// declaration order.
func LoadChain(fn http.HandlerFunc, configs []Conf) http.HandlerFunc {
	for i := (len(configs) - 1); i > -1; i-- {
		m := configs[i]
		fn = Load(m.Name, fn, m.Options)

	}
	return fn
}
