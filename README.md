# Middleman [![Go Report Card](https://goreportcard.com/badge/gitlab.com/npolar/middleware/middleman)](https://goreportcard.com/report/gitlab.com/npolar/middleware/middleman)

Middleman is a minimal golang package that defines interfaces for middleware
configuration and linking. It aims to make it easy to load and configure
multiple middlewares in a modular and re-usable manner.

|Branch|Pipeline|Coverage|
|---|---|---|
|[**Master**](https://gitlab.com/npolar/middleware/middleman/tree/master)|[![pipeline status](https://gitlab.com/npolar/middleware/middleman/badges/master/pipeline.svg)](https://gitlab.com/npolar/middleware/middleman/commits/master)|[![coverage report](https://gitlab.com/npolar/middleware/middleman/badges/master/coverage.svg)](https://gitlab.com/npolar/middleware/middleman/commits/master)|
|[**Develop**](https://gitlab.com/npolar/middleware/middleman/tree/develop)|[![pipeline status](https://gitlab.com/npolar/middleware/middleman/badges/develop/pipeline.svg)](https://gitlab.com/npolar/middleware/middleman/commits/develop)|[![coverage report](https://gitlab.com/npolar/middleware/middleman/badges/develop/coverage.svg)](https://gitlab.com/npolar/middleware/middleman/commits/develop)|

# Installation

```bash
go get gitlab.com/npolar/middleware/middleman
```

# Usage

Full API documentation can be found at [godoc.org](https://godoc.org/gitlab.com/npolar/middleware/middleman).

Middleman can be used to register and load compatible middlewares. Examples of
compatible middlewares can be found in the middleman_test.go file or one of the
reference middlewares:
- [cors](https://gitlab.com/npolar/middleware/cors)

Compatible middlewares can be invoked directly or as part of a chain.
### Direct loading
```go
package main

import (
    "log"
    "net/http"

    "gitlab.com/npolar/middleware/middleman"

    // This registers the cors middleware under the "corsman" name.
    _ "gitlab.com/npolar/middleware/cors"
)

func main() {
    // Setup the chained handler for the "/" path
    http.HandleFunc("/", middleman.Load("corsman", DefaultHandler, nil))
    if err := http.ListenAndServe(":8080", nil); err != nil {
        log.Fatalln(err)
    }
}

// A http.HandlerFunc
func DefaultHandler(w http.ResponseWriter, r *http.Request) {
    // Do something
}
```
### Chain loading
Given the following middleware chain configuration file ```middleware.cfg.json```
```json
[
    {
        "name": "corsman",
        "options":{
            "allowMethods":[
                "GET"
            ]
        }
    }
]
```
We can use the following code to setup a HTTP handler using the previously
defined configuration file.
```go
package main

import (
    "encoding/json"
    "io/ioutil"
    "log"
    "net/http"

    "gitlab.com/npolar/middleware/middleman"

    // This registers the cors middleware under the "corsman" name.
    _ "gitlab.com/npolar/middleware/cors"
)

func main() {
    // Load the middleware configuration
    conf, err := ioutil.ReadFile("middleware.cfg.json")
    if err != nil {
        log.Fatalln(err)
    }

    // Decode the configuration into a middleman.Conf structure
    var mCfg []middleman.Conf
    err = json.Unmarshal(conf, &mCfg)
    if err != nil {
        log.Fatalln(err)
    }

    // Setup the chained handler for the "/" path
    http.HandleFunc("/", middleman.LoadChain(DefaultHandler, mCfg))
    if err = http.ListenAndServe(":8080", nil); err != nil {
        log.Fatalln(err)
    }
}

// A http.HandlerFunc
func DefaultHandler(w http.ResponseWriter, r *http.Request) {
    // Do something
}
```
This way the only thing you need to do to make another middleware available for
usage is add an import statement for a compatible middleware. After that you can
configure it through the JSON file. This makes your HTTP stack easy to extend
without the need for lots of code changes.

# Writing a compatible middleware.

Middleman requires two components to be present in order to use a package as a
middleware. These are:

- An object that implements ```middleman.Middleware``` by having a
```Configure(json.RawMessage) middleman.Link``` method.
- An object that implements ```middleman.Link``` by having a
```Wrap(http.HandlerFunc) http.HandlerFunc``` method.

So the minimal skeleton for a compatible middleware would be something like

```go
package mymware

import (
    "encoding/json"
    "net/http"

    "gitlab.com/npolar/middleware/middleman"
)

const (
    name = "myMware"
)

type MyMiddleware struct{}

func (mm MyMiddleware) Configure(opts json.RawMessage) middleman.Link {
    ml := &MyLink{}
    json.Unmarshal(opts, ml)
    return ml
}

type MyLink struct{}

func (ml *MyLink) Wrap(fn http.HandlerFunc) http.HandlerFunc {
    return func(w http.ResponseWriter, r *http.Request) {
        // Do something here before calling the wrapped function.
        fn(w, r)
    }
}

// By adding the registration logic to the init call we can make our middleware
// register itself with any project using the middleman library. Adding the
// following import statement: '_ "gitlab.com/devman/mymware"', will make it
// available for usage with the "myMware" name.
func init() {
    middleman.RegisterMiddleware(name, MyMiddleware{})
}
```
# Notice
Middleman is still under development, API changes and bugs are likely. Usage in
production should be avoided for the time being.
