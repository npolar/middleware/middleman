package middleman

import (
	"encoding/json"
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"
)

const (
	name    = "testMiddleware"
	confMsg = "Just a link in the chain"
)

// TestMiddleware serves as an anch
type TestMiddleware struct{}

func (tm TestMiddleware) Configure(opts json.RawMessage) Link {
	// Initialize the middleware
	var tl = &TestLink{}

	// Configure the middleware
	json.Unmarshal(opts, tl)

	return tl
}

// TestLink defines a Message field that can be used to set a custom response
// message to the caller.
type TestLink struct {
	Message string `json:"message,omitempty"`
}

// Wrap implements the Link interface defined by middleman.
func (tl *TestLink) Wrap(fn http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		// The middleware writes the message to the http.ResponseWriter when
		// the request method is GET and the message isn't blank.
		if r.Method == "GET" && tl.Message != "" {
			w.Header().Set("Content-Type", "text/plain;charset=UTF-8")
			w.WriteHeader(http.StatusOK)
			fmt.Fprintf(w, tl.Message)
		} else {
			// Otherwise call the function being wrapped
			fn(w, r)
		}
	}
}

// NoopHandler returns a NOOP test on every call
func NoopHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "text/plain;charset=UTF-8")
	w.WriteHeader(http.StatusOK)
	fmt.Fprint(w, "NOOP")
}

func TestMiddleman(t *testing.T) {
	t.Run("Registration", func(t *testing.T) {

		// Register the test middleware with middleman.
		RegisterMiddleware(name, TestMiddleware{})

		t.Run("Panic:NilInstance", func(t *testing.T) {
			defer func() {
				if r := recover(); r == nil {
					t.Fatal("Should panic when called with nil middleware")
				}
			}()

			// A second call with the same name should result in a panic
			RegisterMiddleware(name, nil)
		})

		t.Run("Panic:DupName", func(t *testing.T) {
			defer func() {
				if r := recover(); r == nil {
					t.Fatal("Should panic when called on same name")
				}
			}()

			// A second call with the same name should result in a panic
			RegisterMiddleware(name, TestMiddleware{})
		})
	})

	// List available middlewares
	t.Run("List", func(t *testing.T) {
		if len(Middlewares()) < 1 {
			t.Fatal("Should have at least one available middleware.")
		}
	})

	// Load Middleware
	t.Run("Load", func(t *testing.T) {
		// Wrap our original handler with our test middleware.
		var wrappedFunc = Load(name, NoopHandler,
			[]byte(`{"message": "`+confMsg+`"}`),
		)

		// Setup a test request
		req, err := http.NewRequest("GET", "/", nil)
		if err != nil {
			t.Fatal(err)
		}

		// Setup a response recorder and call our wrapped handler.
		rr := httptest.NewRecorder()
		wrappedFunc.ServeHTTP(rr, req)

		if rr.Body.String() != confMsg {
			t.Fatalf("Should return: %s, got -> %s", confMsg, rr.Body.String())
		}

		rr.Body.Reset()
		wrappedFunc = Load("unknown", NoopHandler, nil)
		wrappedFunc.ServeHTTP(rr, req)

		if rr.Body.String() != "NOOP" {
			t.Fatalf("Should use the original function when loaded with unknown middleware.")
		}
	})

	t.Run("LoadChain", func(t *testing.T) {
		var confs = []Conf{
			{Name: name, Options: []byte(`{"message": "` + confMsg + `"}`)},
			{Name: "unknown"},
		}

		// Wrap our original handler with our test middleware.
		var wrappedFunc = LoadChain(NoopHandler, confs)

		// Setup a test request
		req, err := http.NewRequest("GET", "/", nil)
		if err != nil {
			t.Fatal(err)
		}

		// Setup a response recorder and call our wrapped handler.
		rr := httptest.NewRecorder()
		wrappedFunc.ServeHTTP(rr, req)

		if rr.Body.String() != confMsg {
			t.Fatalf("Expected to see: %s, got -> %s", confMsg, rr.Body.String())
		}
	})
}
